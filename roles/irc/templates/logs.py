#!/usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# {{ ansible_managed }}
#
# Copyright (C) 2014 Mattia Rizzolo <mattia@mapreri.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# On Debian systems, the complete text of the GNU General
# Public License can be found in `/usr/share/common-licenses/GPL-3'.


"""
TODO:
* the handling of log filenames should definitely be more flexible
* paths should be far more uniform
* implement something to translate xchat logs format in energymech format
  (to be hable to submit it to pisg (and dunno if irc2html is able to
  handle it anyway))
"""

import os
import re
import sys
import json
import signal
import logging
import traceback
from shutil import copy, move
from string import Template
from subprocess import call
from datetime import datetime

basepath = "/srv/irc.mapreri.org"

originlog = "/srv/znc/moddata/log"
logdir = "/srv/znc/logs"
finaldir = basepath + "/htdocs/logs"
statsdir = basepath + "/htdocs/stats"
privatelogs = basepath + "/htdocs/privatelogs"
errorlogs = basepath + "/htdocs/errorlogs"
settings_file = basepath + "/bin/settings"
pisgcfg = basepath + "/bin/pisg.cfg"
pisgd = basepath + "/bin/pisg.d"

encodings = ['utf8', 'latin1']

class loghandler:

    def load_settings(self):
        with open(settings_file, "r") as fd:
            settings = json.load(fd)
        # rewrite the file to assure the indentation is correct
        with open(settings_file, "w") as fd:
            json.dump(settings, fd, sort_keys=True, indent=4)
            # put a newline at the EOF
            fd.write("\n")
        return settings

    def load_logs(self):
        origin = os.listdir(logdir)
        if len(origin) == 0:
            logger.info("No logs to parse available")
            return []
        # the filename is something like
        # mapreri_oftc_#debian-devel_20141126.log
        # be aware! it could also be (yeah! bad people...)
        # mapreri_oftc__rene__20141203.log
        logs = []
        username = False
        if len(origin[0].split('_')) >= 4: # XXX this is not optimal for the second example
            username = True
        for log in origin:
            log_data = {}
            log_data["filename"] = log
            log = log.rsplit('_', 1)
            log_data["date"] = log[-1]
            log.pop()
            if len(log) != 1:
                logger.critical("Something bad happened...")
                sys.exit(10)
            if username:
                log = log[0].split('_', 1)
                log_data['username'] = log[0]
                log.pop(0)
            log = log[0].split('_', 1)
            log_data["network"] = log[0]
            log_data["channel"] = log[1]
            logs.append(log_data)
        for log in logs:
            # log['date'] is something like 20141126.log
            log['date'] = log['date'].split(".")[0]
            try:
                log['date'] = datetime.strptime(log['date'], "%Y%m%d")
                dashed = False
            except ValueError:
                log['date'] = datetime.strptime(log['date'], "%Y-%m-%d")
                dashed = True
            log['date'] = datetime.strftime(log['date'], "%Y %m %d").split()
            logger.debug("log: " + str(log))
        return logs

    def find_and_move(self):
        find = "find " + originlog + " -type f -exec mv {} " + logdir + " \;"
        logger.debug("find command: " + find)
        call([find], shell=True)

    def call_pisg(self):
        os.makedirs("/tmp/pisg_cache", exist_ok=True)
        logger.info("Calling pisg...")
        call(["pisg", "--silent", "-co", pisgcfg])

    def sort_logs(self, log):
        sortedlog = log+"+sorted"
        logger.info("\tsorting log " + log + "...")
        with open(log, "r") as origin, \
             open(sortedlog, "a") as final:
            # omit empty lines and lines containing only whitespace
            lines = [line for line in origin if line.strip()]
            origin.close()
            lines.sort()
            final.writelines(lines)
        logger.debug("\trename " + sortedlog + " → " + log)
        os.rename(sortedlog, log)

    def unicodeerrorlog(self, line, log):
        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")
        errordir = errorlogs + "/unicodedecodeerror/" + now
        dst = errordir + "/" + log['filename']
        errorstr = "UnicodeDecodeError! Moving the item to a staging location."
        logger.error("\t" + errorstr)
        logger.error("\tManual handling required: " + dst)
        try:
            os.makedirs(errordir, mode=0o775, exist_ok=True)
        except OSError as e:
            if e.errno != 17:
                # XXX This error should not exist, because we use
                # exist_ok=True, but here it is...
                logger.critical("OSError: " + str(e))
                raise
        with open(dst, 'wb') as erroroutput:
            erroroutput.writeline(line)
        url = "http://irc.mapreri.org/errorlogs/unicodedecodeerror/" + now  + \
              "/" + log['filename'].replace("#", "%23")
        logger.error("\tCheck it out at " + url)

    def errorlog(self, log, ratio, quiet=False):
        src = logdir + '/' + log['filename']
        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")
        errordir = errorlogs + "/" + ratio + "/" + now
        dst = errordir + "/" + log['filename']
        errorstr = ratio + "! Moving the item to a staging location."
        if not quiet:
            logger.error("\t" + errorstr)
            logger.error("\tManual handling required: " + dst)
        try:
            os.makedirs(errordir, mode=0o775, exist_ok=True)
        except OSError as e:
            if e.errno != 17:
                # XXX This error should not exist, because we use
                # exist_ok=True, but here it is...
                logger.critical("OSError: " + str(e))
                raise
        if safe:
            copy(src, errordir)
        else:
            move(src, errordir)
        url = "http://irc.mapreri.org/errorlogs/" + ratio + "/" + now  + \
              "/" + log['filename'].replace("#", "%23")
        if not quiet:
            logger.error("\tCheck it out at " + url)

    def decode(self, line, logfile=None):
        for encoding in encodings:
            if encoding != "utf8":
                if logfile == None:
                    msg = "non-utf8 string detected: " + str(line) + \
                           "Trying " + encoding + "."
                else:
                    msg = "non-utf8 string detected on " + logfile['channel'] + \
                      "@" + logfile['network'] + " (trying " + encoding  + \
                      '): "' + str(line) + '"'
                logger.warning("\t" + msg)
            try:
                return line.decode(encoding)
            except UnicodeDecodeError:
                pass
        logger.error("\tNon decodable string. Giving up.")
        if not logfile == None:
            self.unicodeerrorlog(line, logfile)
        return ''

    def handle_dup(self, dups):
        """
        This method checks the duplicated logs to find out if they are
        effectively duplicated, parsing one line at time (excluding the
        timestamp), and do things (e.g. call handle() to to store the log).
        Currently it's able to handle only duplicated logs, not triplicated or
        more.
        """
        goodlogs = [] # contains the list of equals logs
        # contains a filename list of log to be removed (because they are
        toremove = []                                   # correctly duplicated)
        for duplog in dups:
            logger.info("handling... " + str(duplog))
            log1 = []
            log2 = []
            if len(duplog['users']) == 2:
                # first check if the lenght is different
                with open(logdir + '/' + duplog['filenames'][0], 'rb') as fd:
                    len1 = sum(1 for line in fd)
                with open(logdir + '/' + duplog['filenames'][1], 'rb') as fd:
                    len2 = sum(1 for line in fd)
                if len1 != len2:
                    msg = "\tthese two duplicated files have different lenght"
                    logger.error(msg)
                    logger.error("\t\tduplog: " + str(duplog))
                    badlog1 = {'filename': duplog['filenames'][0]}
                    self.errorlog(badlog1, 'DuplicatedLogChecked')
                    badlog2 = {'filename': duplog['filenames'][1]}
                    self.errorlog(badlog2, 'DuplicatedLogChecked')
                    continue
                # strip out timestamp from the lines
                with open(logdir + '/' + duplog['filenames'][0], 'rb') as fd:
                    for line in fd:
                        dline = self.decode(line)
                        log1.append(re.split('^\[\d\d:\d\d:\d\d\] ', dline)[1])
                with open(logdir + '/' + duplog['filenames'][1], 'rb') as fd:
                    for line in fd:
                        dline = self.decode(line)
                        log2.append(re.split('^\[\d\d:\d\d:\d\d\] ', dline)[1])
                # then actually check the content (without timestamp)
                i = 0
                same_line = True
                while i < len1:
                    if log1[i] != log2[i]:
                        logger.error("\tthese two duplicated files actually differs")
                        logger.error("\t\tduplog: " + str(duplog))
                        logger.debug("\t\tlog1: " + log1[i])
                        logger.debug("\t\tlog2: " + log2[i])
                        badlog1 = {'filename': duplog['filenames'][0]}
                        self.errorlog(badlog1, 'DuplicatedLogChecked')
                        badlog2 = {'filename': duplog['filenames'][1]}
                        self.errorlog(badlog2, 'DuplicatedLogChecked')
                        same_line = False
                        break
                    i = i + 1
                if not same_line:
                    continue
                goodlog = {'filename': duplog['filenames'][0],
                           'channel': duplog['channel'],
                           'network': duplog['network'],
                           'date': duplog['date']}
                logger.info("\tThe log " + str(goodlog) + " (and copy) is fine")
                goodlogs.append(goodlog)
                toremove.append(logdir + '/' + duplog['filenames'][1])
            else:
                logger.warning("\tI cannot handle more than 2 users for the same channel")
                i = 0
                while i < len(duplog['users']):
                    badlog = {'filename': duplog['filenames'][i]}
                    self.errorlog(badlog, 'DuplicatedLog3Users', quiet=True)
                    i = i + 1
        for log in goodlogs:
            logger.info("Iterating over duplicated logs...: " + log['channel'] +
                        "@" + log['network'])
            self.handle(log)
        for logfile in toremove:
            if not safe:
                logger.debug("\tremoving a duplicated file: " + logfile)
                os.remove(logfile)

    def pisg_config(self, log):
        if log['network'] == 'default':
            # I don't want to cumpute the stats for the "default" network (aka
            # Acn0w's freenode)
            logger.info("\tNot computing stats for the \"default\" network")
            return
        pisg_conf_file = pisgd + "/" + log['network'] + "_" + log['channel'] + \
                                                                        ".cfg"
        pisg_stats_dir = statsdir + "/" + log['network']
        logger.debug("\tpisg_conf_file: " + pisg_conf_file)
        logger.debug("\tpisg_stats_dir: " + pisg_stats_dir)

        # check if the channel conf file exists, otherwise do things
        if not os.access(pisg_conf_file, os.W_OK):
            pisg_template = Template("""
<channel="$channel">
    Logfile = "$logdir/$network/\$channel/*/*.log"
    Network = "$network"
    OutputFile = "$statsdir/$network/$channelname.html"
</channel>
""")
            pisg_chan_conf = pisg_template.substitute(logdir=finaldir,
                                                      statsdir=statsdir,
                                                      network=log['network'],
                                                      channel=log['channel'],
                                    channelname=log['channel'].replace('#', ''))
            logger.debug("\tpisg_conf_file: " + pisg_conf_file)
            with open(pisg_conf_file, 'w') as f:
                f.writelines(pisg_chan_conf)
            # add an include line to the pisg conf file, if it's not already there
            with open(pisgcfg, 'r') as pisg_conf:
                regexp = str(log['network']) + "_" + \
                         str(log['channel']).replace('#', '\#') + "\.cfg"
                config_found = False
                for line in pisg_conf:
                    if re.search(regexp, line):
                        config_found = True
                        break
            if not config_found:
                with open(pisgcfg, 'a') as pisg_conf_a:
                    confline = '<include="' + pisg_conf_file + '">\n'
                    logger.info("\tadding " + confline.replace('\n', '') +
                                 " to " + pisgcfg)
                    pisg_conf_a.write(confline)
            else:
                logger.debug("\tpisg configuration already there, " +
                                  "doing nothing.")
        #check if the final directory for the stats exists, otherwise do things
        if not os.access(pisg_stats_dir, os.W_OK):
            try:
                logger.info("\tcreating directory for pisg stats: " +
                              pisg_stats_dir)
                os.makedirs(pisg_stats_dir, mode=0o775, exist_ok=True)
            except OSError as e:
                if e.errno != 17:
                    # XXX This error should not exist, because we use
                    # exist_ok=True, but here it is...
                     logger.critical("OSError: " + str(e))
                     raise

    def handle(self, log, dups_chans=[]):
        """
        Do the actual handling of the log.

        Every log dictionary has at least these keys:
          * filename: the log filename (without the path)
          * channel: this could be either the channel name (including '#') or
                     nickname in a query (so without the '#' bit)
          * network: the network name setted in znc
          * date: a list made of
            + year
            + month
            + date
        And can have (depends):
          * username: the username of the znc user

        The format of dups:
        [{'channel': '#ubuntu-it-web', 'users': ['mapreri', 'deshack']},
         {'channel': '#bubu', 'users': ['mapreri', 'Acn0w']}]
        """
        logger.info("\titem: " + str(log))
        blacklisted_chans = self.settings['blacklist']['channels']
        dirname = finaldir + "/" + log['network'] + "/" + log['channel'] + \
                  "/" + log['date'][0]
        src = logdir + "/" + log['filename']
        logger.debug("\tsrc: " + src)
        dst = dirname + "/" + log['channel'] + "_" + log['date'][0] + "-" + \
              log['date'][1] + "-" + log['date'][2] + ".log"

        is_dup = False
        is_private = False
        if log['network'] in self.settings['blacklist']['servers']:
            # this network should not be publicly logged
            is_private = True
            logger.debug("\tThe item is private.")
            now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            dirname = privatelogs + '/' + now
            dst = dirname + '/' + log['filename']
        elif log['channel'][0] != '#' or log['channel'] in blacklisted_chans:
            # this is a query or is a blacklisted chan, put the stats somewhere
            # safe (actually a directory accesible only from an ip of mine)
            is_private = True
            logger.debug("\tThe item is private.")
            now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            dirname = privatelogs + '/' + now
            dst = dirname + '/' + log['filename']
        elif log['channel'] in dups_chans:
            # this channel is logged by two or more people
            logger.info("\tThe item is a duplicate of another one")
            is_dup = True
        if not is_dup:
            logger.debug("\tdst: " + dst)
            logger.info("\t" + src + " → " + dst)
            try:
                os.makedirs(dirname, mode=0o775, exist_ok=True)
            except OSError as e:
                if e.errno != 17:
                    # XXX This error should not exist, because we use
                    # exist_ok=True, but here it is...
                     logger.critical("OSError: " + str(e))
                     raise
            try:
                with open(src, "rb") as origlog, \
                     open(dst, "a") as finallog:
                    finallog.writelines(self.decode(line, log) for line in origlog)
            except:
                raise
        else: # the item is_dup:
           #self.errorlog(log, 'DuplicatedLog')
           # now I try to really handle duplicated logs....
           pass
        if is_dup:
            logger.debug("\tthe item is duplicated. Not removing the logfile")
        elif safe:
            logger.info("\tscript called with the \"safe\" option. " +
                        "Not removing the origin file")
        else:
            logger.debug("\tdeleting original log " + src + "...")
            os.remove(src) # Finally, remove the origin file.
        if not is_private and not is_dup:
            self.sort_logs(dst)
            logger.info("\tlogs2html...")
            call(["/usr/local/bin/logs2html", dirname])
            logger.info("\tgenerating pisg configuration...")
            self.pisg_config(log)

    def _find_dupped_chans(self, logs):
        duplicated_logs = []
        dupped = []
        total = len(logs)
        i = 0
        # check whether the same channel in the same network is present in two
        # or more users.
        while i < total:
            if i in dupped: # already checked
                i = i + 1
                continue
            dup_info = {'channel': logs[i]['channel'], 'date': logs[i]['date'],
                        'network' : logs[i]['network'],
                        'users': [logs[i]['username']],
                        'filenames': [logs[i]['filename']]}
            if logs[i]['channel'][0] != '#': # query, don't treat it as dup
                i = i + 1
                continue
            j = i + 1
            while j < total:
                if j in dupped: # already checked
                    j = j + 1
                    continue
                if logs[i]['network'] == logs[j]['network'] and \
                   logs[i]['channel'] == logs[j]['channel'] and \
                   logs[i]['date'] == logs[j]['date']: # it's a dup
                    dup_info['users'].append(logs[j]['username'])
                    dup_info['filenames'].append(logs[j]['filename'])
                    dupped.append(j)
                j = j + 1
            if len(dup_info['users']) > 1:
                date = dup_info['date'][0] + '-' + dup_info['date'][1] + '-' \
                       + dup_info['date'][2]
                logger.info("\tThe channel " + dup_info['channel'] + "@" +
                            dup_info['network'] + " for " + date +
                            " is duplicated")
                logger.debug("\t\t=> " + str(dup_info))
                duplicated_logs.append(dup_info)
                dupped.append(i)
            i = i + 1
        return duplicated_logs

    def __init__(self):
        lock()
        self.settings = self.load_settings()
        if not repick:
            self.find_and_move()
        else:
            logger.debug("Script called with the \"repick\" option. Not" +
                          " running the find_and_move function")
        logs = self.load_logs()
        duplicated_chans = self._find_dupped_chans(logs)
        dups_chans = []
        for item in duplicated_chans:
            dups_chans.append(item['channel'])
        i = 0
        total = len(logs)
        while i < total:
            logger.info("Iterating... " + str(i+1) + "/" + str(total) + ":")
            self.handle(logs[i], dups_chans)
            i = i + 1
        if len(duplicated_chans) > 0:
            logger.info("Handling duplicated channels...")
            self.handle_dup(duplicated_chans)
        if not repick and not nopisg:
            self.call_pisg()
        elif repick:
            logger.debug("Script called with the \"repick\" option. Not" +
                          " running pisg")
        elif nopisg:
            logger.debug("Script called with the \"nopisg\" option. Not" +
                          " running pisg")
        unlock()
        logger.info('And also this time all is finished "well" :)')


def lock():
    logger.debug('Locking the state...')
    with open(basepath + '/loghandler.lock', 'w') as fd:
        text = 'locked by the loghandler process at ' + str(datetime.now())
        logger.info(text)
        fd.write(text)
def unlock():
    logger.debug('Unlocking the state...')
    try:
        os.remove(basepath + '/loghandler.lock')
    except:
        raise

def handle_uncaught_exceptions(ex_cls, ex, tb):
    logger.critical('Uncaught Exception:\n' + ''.join(traceback.format_tb(tb)))
    logger.critical('{0}: {1}'.format(ex_cls, ex))

# catch ctrl+c
def signal_handler(signal, frame):
    logger.warning(datetime.now().strftime("%Y-%m-%d %H:%M:%S") +
                ": Keyboard Interrupt. Exiting...")
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# catch a couple of command line options
safe = False
debug = False
nopisg = False
repick = False
if len(sys.argv) > 1:
    if 'safe' in sys.argv:
        safe = True
    if 'debug' in sys.argv:
        debug = True
    if 'nopisg' in sys.argv:
        nopisg = True
    if 'repick' in sys.argv:
        repick = True

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
if not debug: # file handler for debug messages, stream handler for errors
    fh = logging.FileHandler(basepath + "/logs/loghandler.log")
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
else:  # output all messages both to file and console
    fh = logging.FileHandler(basepath + "/logs/loghandler.log")
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s:\t%(message)s', "%Y%m%dT%H:%M:%SZ")
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

# do things when an exception occur
sys.excepthook = handle_uncaught_exceptions


if __name__ == '__main__':
    logger.debug("logdir:\t\t" + logdir)
    logger.debug("finaldir:\t" + finaldir)
    logger.debug("privatelogs:\t" + privatelogs)
    logger.debug("errorlogs:\t" + errorlogs)
    logger.debug("settings_file:\t" + settings_file)
    logger.debug("pisgcfg:\t" + pisgcfg)
    logger.debug("pisgd:\t\t" + pisgd)
    logger.debug("statsdir:\t" + statsdir)

    if os.access(basepath + '/loghandler.lock', os.F_OK):
        logger.critical('There is a lock at ' + basepath + '/loghandler.lock' +
                        ' Aborting the process now!')
        sys.exit(10)

    loghandler()

