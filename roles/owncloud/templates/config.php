<?php
$CONFIG = array (
  'instanceid' => 'ocaf7e7d23f9',
  'passwordsalt' => '77a23ef1592f1bc29a016366b39e22',
  'trusted_domains' => 
  array (
    0 => 'cloud.mapreri.org',
  ),
  'datadirectory' => '/usr/share/owncloud/data',
  'dbtype' => 'pgsql',
  'version' => '7.0.5.2',
  'appstoreenabled' => false,
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/usr/share/owncloud/apps',
      'url' => '/apps',
      'writable' => false,
    ),
  ),
  'dbname' => 'owncloud',
  'dbhost' => '192.168.57.1',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'owncloud',
  'dbpassword' => '{{ dbpassword }}',
  'installed' => true,
  'forcessl' => true,
  'theme' => '',
  'maintenance' => false,
  'mail_smtpmode' => 'smtp',
  'mail_smtphost' => '192.168.57.1',
  'mail_smtpport' => '25',
  'mail_smtptimeout' => 10,
  'mail_smtpname' => '',
  'mail_smtppassword' => '',
  'mail_from_address' => 'master',
  'mail_domain' => 'mapreri.org',
  'loglevel' => '1',
  'secret' => '2324ea233f0f9d2c3812beb599a14c9852dd12b781caa3db84d28630a91588dea79795a069ec0e9282ae4776e23489b6',
);
