#!/usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# {{ ansible_managed }}
#
# Copyright (C) 2014 Mattia Rizzolo <mattia@mapreri.org>
#
# License: BSD-3-clause
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the author nor the names of other contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# .
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import os
import sys
import json
import logging
from subprocess import check_output, STDOUT

logging.basicConfig(format="%(levelname)s: %(message)s",
                    filename="/var/log/iptables-generate.log",
                    level=logging.INFO)
#                    level=logging.DEBUG)

conffile = '/etc/iptables.json'
ipv4tables = '/sbin/iptables'
ipv6tables = '/sbin/ip6tables'
hostname = '{{ ansible_hostname }}'

cmd_before = [
    # default policies: we'll drop the leftovers at the end
    ['-P', 'INPUT', 'ACCEPT'],
    ['-P', 'OUTPUT', 'ACCEPT'],
    ['-P', 'FORWARD', 'ACCEPT'],
    # flish all current values
    ['-F', 'INPUT'],
    ['-F', 'OUTPUT'],
    ['-t', 'nat', '-F', 'PREROUTING'],
    ['-t', 'nat', '-F', 'POSTROUTING'],
    # accept connections already established
    ['-A', 'INPUT', '-m', 'conntrack', '--ctstate', 'ESTABLISHED,RELATED',
        '-j', 'ACCEPT'],
    # accept everything on the loopback device
    ['-I', 'INPUT', '1', '-i', 'lo', '-j', 'ACCEPT']
    ]
cmd_after = [
    # log denied connections (to the syslog)
    # ['-I', 'INPUT', '2', '-m', 'limit', '--limit', '5/min', '-j', 'LOG',
    #      '--log-prefix', '"iptables', 'denied:', '"', '--log-level', '7'],
    # drop everything that doesn't match the given rules
    ['-A', 'INPUT', '-j', 'DROP']
    ]


def load_settings():
    with open(conffile, "r") as fd:
        rules = json.load(fd)
    # remove the _comment entry
    if len(rules[0]) == 1 and '_comment' in rules[0]:
        rules.pop(0)
    return rules


def gen_command(rule):
    if 'port' in rule and 'protocol' in rule:
        cmd = ['-A', 'INPUT', '-p', rule['protocol'], '--dport',
               str(rule['port'])]
        if 'from' in rule:
            cmd.extend(['-s' , rule['from']])
        cmd.extend(['-j', 'ACCEPT'])
    else:
        try:
            cmd = ['-t', rule['table'], '-A', rule['chain']]
            cmd.extend(['-j', rule['target']])
            cmd.extend(rule['rules'].split())
        except KeyError as e:
            msg = "KeyError: the mandatory option " + e + " is not " + \
                  "defined in the rules table"
            logging.critical(msg)
            print(msg)
            sys.exit(10)
    return cmd


def call_external_program(cmd):
    logging.info("calling: " + str(cmd))
    try:
        output = check_output(cmd, stderr=STDOUT)
    except:
        logging.critical('Calling external command failes: ' + ' '.join(cmd))
        raise
    if len(output) > 1:
        msg = "iptables generated output: " + output + "\nCommand launched: " \
              + ' '.join(cmd)
        logging.error(msg)
        print(msg)
        sys.exit(5)


def init():
    rules = load_settings()
    logging.debug(json.dumps(rules, indent=4))
    if len(sys.argv) > 1 and sys.argv[1] == 'also_ipv6':
        executables.append('/sbin/iptables6')
    for exe in (ipv4tables, ipv6tables):
        for command in cmd_before:
            cmd = [exe] + command
            call_external_program(cmd)
        for rule in rules:
            ipcheck = False
            hostcheck = False
            if rule.get('type'):
                if rule['type'] == 'both':
                    ipcheck = True
                elif rule['type'] == '4' and exe == ipv4tables:
                    ipcheck = True
                elif rule['type'] == '6' and exe == ipv6tables:
                    ipcheck = True
            else:
                log.warning('rule ' + rule['name'] + ' does not have a type ' +
                            'field specifying the IP version. The rule has ' +
                            'been skipped.')
            if 'restrict_to_hosts' in rule and \
                    len(rule['restrict_to_hosts']) > 0:
                if hostname in rule['restrict_to_hosts']:
                    hostcheck = True
                else:
                    logging.info('rule ' + rule['name'] + ' is only for' +
                                 str(rule['restrict_to_hosts']))
            elif 'restrict_to_hosts' not in rule or \
                    len(rule['restrict_to_hosts']) == 0:
                hostcheck = True
            if hostcheck and ipcheck:
                cmd_opts = gen_command(rule)
                cmd = [exe] + cmd_opts
                call_external_program(cmd)
        for command in cmd_after:
            cmd = [exe] + command
            call_external_program(cmd)


if __name__ == '__main__':
    init()
