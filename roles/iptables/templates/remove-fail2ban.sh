#!/bin/bash
#
# {{ ansible_managed }}
#

set -xe

IP=$1

# iptables:
iptables -D fail2ban-default -s $IP -j DROP

# hosts.deny
sed -i.old /ALL:\ $IP/d /etc/hosts.deny
